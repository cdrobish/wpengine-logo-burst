varying vec4 vColor;
varying float vLife;

void main() {
    vec3 outgoingLight = vColor.rgb;
    // outgoingLight *= shadowMask;//pow(shadowMask, vec3(0.75));
    gl_FragColor = vec4( outgoingLight, 0.0 );
}

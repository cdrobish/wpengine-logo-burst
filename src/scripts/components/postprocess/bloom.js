import Effect from './effect';
import fbo from './fbo';
import bloomFrag from 'glsl/bloom.frag';
// import bloomBlurShader from 'materials';
import composer from './composer';
import {resolution} from './composer';
import {prefix} from 'components/view';
import bloomBlurFrag from 'glsl/bloomBlur.frag';
import postQuadVert from 'glsl/postQuad.vert';

import settings from 'core/settings';

const prefixStr = prefix || 'precision mediump float;\n';

const BLUR_BIT_SHIFT = 1;
let undef;

const bloomBlurShader = new THREE.RawShaderMaterial({
    uniforms: {
        u_texture: { type: 't', value: undef },
        u_delta: { type: 'v2', value: new THREE.Vector2() }
    },
    vertexShader: postQuadVert,
    fragmentShader: prefixStr + bloomBlurFrag
});


export default class Bloom extends Effect {
  constructor(){
    super({
      name:'bloom',
      uniforms: {
            u_blurTexture: { type: 't', value: undef },
            u_amount: { type: 'f', value: 0 }
        },
        fragmentShader: bloomFrag
    });
  }
  render(delta, target, toScreen) {
    // console.log(bloomBlurShader)

    let {
      enabled,
      blurX,
      blurY,
      amount
    } = settings.bloom;

    let tmpTarget1 = composer.getTarget(BLUR_BIT_SHIFT);
    let tmpTarget2 = composer.getTarget(BLUR_BIT_SHIFT);

    composer.releaseTarget(tmpTarget1, tmpTarget2);

    bloomBlurShader.uniforms.u_texture.value = target;
    bloomBlurShader.uniforms.u_delta.value.set(blurX / resolution.x, 0);
    fbo.render(bloomBlurShader, tmpTarget1);

    bloomBlurShader.uniforms.u_texture.value = tmpTarget1;
    bloomBlurShader.uniforms.u_delta.value.set(0, blurY / resolution.y);
    fbo.render(bloomBlurShader, tmpTarget2);

    this.uniforms.u_blurTexture.value = tmpTarget2;
    this.uniforms.u_amount.value = amount;

    super.render.call(this, delta, target, toScreen);
  }
}

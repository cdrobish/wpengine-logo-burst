import {
  TweenMax,
  TimelineMax,
  Back,
  Quad,
  Expo,
  Cubic} from 'gsap';
import settings from 'core/settings';

const timeline = () => {
  let ease = Cubic.easeInOut;

  let {
    logo,
    emitter,
    behavior,
    bloom,
    motionBlur,
    rendering
  } = settings;

  const tl = new TimelineMax({});

  // move
  let bloomTl = new TimelineMax();
  bloomTl.to(bloom, 0.2, {amount:1.4});
  bloomTl.to(bloom, 0.1, {amount:1.6});
  bloomTl.to(bloom, 0.3, {amount:2});
  bloomTl.to(bloom, 0.1, {amount:1.2});
  bloomTl.to(bloom, 0.2, {amount:2.8});
  bloomTl.to(bloom, 0.4, {amount:1});
  bloomTl.to(bloom, 0.2, {amount:1.6});
  bloomTl.to(bloom, 0.2, {amount:1.4});
  bloomTl.to(bloom, 0.1, {amount:1.6});
  bloomTl.to(bloom, 0.3, {amount:2});
  bloomTl.to(bloom, 0.1, {amount:1.2});
  bloomTl.to(bloom, 0.2, {amount:2.8});
  bloomTl.to(bloom, 0.4, {amount:1});
  bloomTl.to(bloom, 0.2, {amount:1.6});
  bloomTl.to(bloom, 0.2, {amount:1.4});
  bloomTl.to(bloom, 0.1, {amount:1.6});
  bloomTl.to(bloom, 0.3, {amount:2});
  bloomTl.to(bloom, 0.1, {amount:1.2});
  bloomTl.to(bloom, 0.2, {amount:2.8});
  bloomTl.to(bloom, 0.4, {amount:1});
  bloomTl.to(bloom, 0.2, {amount:1.6});


  let time = 1.3;
  let pulse = new TimelineMax();
  pulse.to(behavior, time, {attractionFactor:10, dieSpeed:0, ease:Cubic.easeInOut});
  pulse.to(behavior, time, {attractionFactor:-5, curlStrength:30, dieSpeed:0.4, ease:Cubic.easeInOut});
  pulse.to(behavior, time, {attractionFactor:0, dieSpeed:0.4, ease:Cubic.easeInOut}, '+=0.25');
  pulse.to(behavior, 0.5, {attractionFactor:0, dieSpeed:0.8, ease:Cubic.easeInOut}, '+=0.25');

  let timeTL = new TimelineMax();
  timeTL.to(rendering, 0.3, {timeScale:0.05, ease:Cubic.easeInOut}, '+=2');
  timeTL.to(rendering, 0.3, {timeScale:0.1, ease:Cubic.easeInOut}, '+=0.25');
  // timeTL.to(rendering, time, {timeScale:0.1, ease:Cubic.easeInOut}, '+=0.25');

  // move.set(emitter, {positionX:0, positionX:0});
  // move.to(emitter, 0.4, {positionZ:0, ease:Cubic.easeInOut}, '+=1');
  // move.to(emitter, 0.4, {positionY:-1000, ease:Cubic.easeInOut}, '+=1');
  // move.set(emitter, {positionY:600, positionX:0});

  // behavior
  //let behave = new TimelineMax();
  //behave.to(behavior, 1, {vecSpeedX:-50, ease:Back.easeInOut}, '+=3')

  tl.add(pulse, 'move+behave');
  tl.add(bloomTl, 'move+behave');
  tl.add(timeTL, 'move+behave');


  return tl;
}

export default timeline;

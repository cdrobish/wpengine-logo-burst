import Effect from './effect';
import fbo from 'components/postprocess/fbo';
import {
  createTarget,
  getColorState,
  setColorState
} from 'components/postprocess/fbo';
import composer from 'components/postprocess/composer';
import {resolution} from 'components/postprocess/composer';
import settings from 'core/settings';
import motionBlurFrag from 'glsl/motionBlur.frag';

import motionBlurLinesVert from 'glsl/motionBlurLines.vert';
import motionBlurLinesFrag from 'glsl/motionBlurLines.frag';
import motionBlurSamplingFrag from 'glsl/motionBlurSampling.frag';

// import {
//   linesMaterial,
//   samplingMaterial
// } from 'materials';

import {
  renderer,
  scene,
  prefix
} from 'components/view';


const sampleCount = '#define SAMPLE_COUNT ' + (settings.motionBlur.sampleCount || 21) + '\n';

const prefixStr = prefix || 'precision mediump float;\n';

// for debug
const skipMatrixUpdate = false;
const targetFPS = 60;

let useSampling = false;
let motionMultiplier = 7;
let maxDistance = 120;
let motionRenderTargetScale = 1;
let linesRenderTargetScale = 1 / 2;
let leaning = 0.5;
let fadeStrength = 1;

// lines method only options
let jitter = 0.5;
let opacity = 1;
let depthBias = 0.002;
let depthTest = false;
let useDithering = false;

let motionTarget = createTarget(1,1,THREE.RGBAFormat,THREE.FloatType);
motionTarget.depthBuffer = false;

let linesTarget = createTarget(1,1,THREE.RGBAFormat,THREE.FloatType);
let linesCamera = new THREE.Camera();
linesCamera.position.z = 1.0;

let linesScene = new THREE.Scene();
let linesGeometry = new THREE.BufferGeometry();

let undef;
let visibleCache = [];
let linesPositions = [];
let linesMaterial;
let samplingMaterial;
let _width, _height;
let linesPositionAttribute;

const getDitheringAmount = (width, height) => {
    if((width & 1) && (height & 1)) {
        return (((width - 1) * (height - 1)) >> 1) + (width >> 1) + (height >> 1);
    } else {
        return (width * height) >> 1;
    }
}

const setObjectBeforeState = (obj) => {
    if(obj.motionMaterial) {
        obj._tmpMaterial = obj.material;
        obj.material = obj.motionMaterial;
        obj.material.uniforms.u_motionMultiplier.value = obj.material.motionMultiplier;
    } else if(obj.material) {
        obj.visible = false;
    }
    visibleCache.push(obj);
}

const setObjectAfterState = (obj) => {
    if(obj.motionMaterial) {
        obj.material = obj._tmpMaterial;
        obj._tmpMaterial = undef;
        if(!settings.motionBlur.skipMatrixUpdate) {
            obj.motionMaterial.uniforms.u_prevModelViewMatrix.value.copy(obj.modelViewMatrix);
        }
    } else {
        obj.visible = true;
    }
}

export default class motionBlur extends Effect {
  constructor(sampleContent){
    super({
      uniforms: {
            u_lineAlphaMultiplier: { type: 'f', value: 1 },
            u_linesTexture: { type: 't', value: linesTarget }
            // u_motionTexture: { type: 't', value: _motionRenderTarget }
        },
        fragmentShader: motionBlurFrag
    });

    let gl = renderer.getContext();
    if(!gl.getExtension('OES_texture_float') || !gl.getExtension('OES_texture_float_linear')) {
         alert('no float linear support');
    }

    linesMaterial = new THREE.RawShaderMaterial({
        uniforms: {
            u_texture: { type: 't', value: undef },
            u_motionTexture: { type: 't', value: motionTarget },
            u_resolution: { type: 'v2', value: resolution },
            u_maxDistance: { type: 'f', value: 1 },
            u_jitter: { type: 'f', value: 0.3 },
            u_fadeStrength: { type: 'f', value: 1 },
            u_motionMultiplier: { type: 'f', value: 1 },
            u_depthTest: { type: 'f', value: 0 },
            u_opacity: { type: 'f', value: 1 },
            u_leaning: { type: 'f', value: 0.5 },
            u_depthBias: { type: 'f', value: 0.01 }


        },
        vertexShader: prefixStr + motionBlurLinesVert,
        fragmentShader: prefixStr + motionBlurLinesFrag,
        blending : THREE.CustomBlending,
        blendEquation : THREE.AddEquation,
        blendSrc : THREE.OneFactor,
        blendDst : THREE.OneFactor ,
        blendEquationAlpha : THREE.AddEquation,
        blendSrcAlpha : THREE.OneFactor,
        blendDstAlpha : THREE.OneFactor,
        depthTest: false,
        depthWrite: false,
        transparent: true
    });

    samplingMaterial = new THREE.RawShaderMaterial({
      uniforms: {
          u_texture: { type: 't', value: undef },
          u_maxDistance: { type: 'f', value: 1 },
          u_fadeStrength: { type: 'f', value: 1 },
          u_motionMultiplier: { type: 'f', value: 1 },
          u_leaning: { type: 'f', value: 0.5 },
          u_motionTexture : { type: 't', value: motionTarget },
          u_resolution: { type: 'v2', value: resolution }
      },
      defines: {
          SAMPLE_COUNT: sampleCount || 21
      },
      fragmentShader: prefixStr + sampleCount + motionBlurSamplingFrag,
      vertexShader : this.vertexShader
    });


    let lines = new THREE.LineSegments(linesGeometry, linesMaterial);
    linesScene.add(lines);
  }

  resize = (width,height) => {
    if(width){
      _width = width;
    }
    if(height){
      _height = height;
    }
    height = height || _height;
    width = width || _width;
    let motionWidth = ~~(width * motionRenderTargetScale);
    let motionHeight = ~~(height * motionRenderTargetScale);

    motionTarget.setSize(motionWidth , motionHeight);

    if(!useSampling) {
        let linesWidth = ~~(width * linesRenderTargetScale),
            linesHeight = ~~(height * linesRenderTargetScale);

        linesTarget.setSize(linesWidth, linesHeight);
        let amount = !useDithering ? linesWidth * linesHeight : getDitheringAmount(linesWidth, linesHeight);
        let currentLen = linesPositions.length / 6;
        if(amount > currentLen) {
            linesPositions = new Float32Array(amount * 6);
            linesPositionAttribute = new THREE.BufferAttribute(linesPositions, 3);
            linesGeometry.removeAttribute('position');
            linesGeometry.addAttribute( 'position', linesPositionAttribute );
        }
        let i,
          i6 = 0,
          x, y,
          size = linesWidth * linesHeight;

        for(i = 0; i < size; i++) {
            x = i % linesWidth;
            y = ~~(i / linesWidth);
            if(!useDithering || ((x + (y & 1)) & 1)) {
                linesPositions[i6 + 0] = linesPositions[i6 + 3] = (x + 0.5) / linesWidth;
                linesPositions[i6 + 1] = linesPositions[i6 + 4] = (y + 0.5) / linesHeight;
                linesPositions[i6 + 2] = 0;
                linesPositions[i6 + 5] = (0.001 + 0.999 * Math.random());
                i6 += 6;
            }
        }
        linesPositionAttribute.needsUpdate = true;
        linesGeometry.drawRange.count = amount * 2;
    }

    this.prevUseDithering = useDithering;
    this.prevUseSampling = useSampling;

    // debugger;

  }

  render = (delta, target, toScreen) => {


    let {
      useDithering,
      useSampling,
      motionMultiplier,
      maxDistance,
      motionRenderTargetScale,
      linesRenderTargetScale,
      leaning,
      fadeStrength,
      jitter,
      opacity,
      depthBias

    } = settings.motionBlur


    if(this.prevUseDithering !== useDithering) {
        this.resize();
    } else if(this.prevUseSampling !== useSampling) {
        this.resize();
    }

    let fpsRatio = 1000 / (delta < 16.667 ? 16.667 : delta) / targetFPS;
    let state = getColorState();

    renderer.setClearColor(0, 1);
    renderer.clearTarget(motionTarget, true);
    scene.traverseVisible(setObjectBeforeState);
    composer.renderScene(motionTarget);

    for(let i = 0, len = visibleCache.length; i < len; i++) {
        setObjectAfterState(visibleCache[i]);
    }
    visibleCache = [];

    if(!useSampling) {

        linesMaterial.uniforms.u_maxDistance.value = maxDistance;
        linesMaterial.uniforms.u_jitter.value = jitter;
        linesMaterial.uniforms.u_fadeStrength.value = fadeStrength;
        linesMaterial.uniforms.u_motionMultiplier.value = motionMultiplier * fpsRatio;
        linesMaterial.uniforms.u_depthTest.value = depthTest;
        linesMaterial.uniforms.u_opacity.value = opacity;
        linesMaterial.uniforms.u_leaning.value = Math.max(0.001, Math.min(0.999, leaning));
        linesMaterial.uniforms.u_depthBias.value = Math.max(0.00001, depthBias);
        linesMaterial.uniforms.u_texture.value = target;

        renderer.setClearColor(0, 0);
        renderer.clearTarget(linesTarget, true);
        renderer.render(linesScene, linesCamera, linesTarget);
    }

    setColorState(state);


    if(useSampling) {
        samplingMaterial.uniforms.u_maxDistance.value = maxDistance;
        samplingMaterial.uniforms.u_fadeStrength.value = fadeStrength;
        samplingMaterial.uniforms.u_motionMultiplier.value = motionMultiplier * fpsRatio;
        samplingMaterial.uniforms.u_leaning.value = Math.max(0.001, Math.min(0.999, leaning));
        samplingMaterial.uniforms.u_texture.value = target;
        composer.render(samplingMaterial, toScreen);
    } else {
        this.uniforms.u_lineAlphaMultiplier.value = 1 + useDithering;
        super.render.call(this, delta, target, toScreen);
    }
  }
}

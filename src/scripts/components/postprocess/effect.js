import _ from 'lodash';
import {
  resolution
} from './composer';
import merge from 'mout/object/merge';
import fbo from 'components/postprocess/fbo';
import composer from 'components/postprocess/composer';
import quadVertexShader from 'glsl/shaderMaterialQuad.vert';

import { prefix } from 'components/view';
const prefixStr = prefix || 'precision mediump float;\n';
// import { copyPostRawShader } from 'materials/';

let undef;

const baseEffectShaderProps = {
  uniforms: {
    u_texture: { type: 't', value: undef },
    u_resolution: { type: 'v2', value: resolution },
    u_aspect: { type: 'f', value: 1 }
  },
  name: 'effect_'+Math.round(Math.random() * 1000000000),
  enabled: true,
  vertexShader: '',
  fragmentShader: '',
  isRawMaterial: true,
  addRawShaderPrefix: true
};

export default class Effect {
  constructor(cfg){
    _.defaultsDeep(this, cfg, baseEffectShaderProps);

    if(!this.vertexShader){
      this.vertexShader = this.isRawMaterial ? fbo.vertexShader : quadVertexShader;
    }
    if(this.addRawShaderPrefix && this.isRawMaterial) {
        this.vertexShader = prefixStr + this.vertexShader;
        this.fragmentShader = prefixStr + this.fragmentShader;
    }

    this.material = new THREE[ this.isRawMaterial ? 'RawShaderMaterial' : 'ShaderMaterial']({
      uniforms : this.uniforms,
      vertexShader : this.vertexShader,
      fragmentShader : this.fragmentShader
    });
  }

  resize = (width,height) => {}

  render(dt, target, toScreen) {
    this.uniforms.u_texture.value = target;
    this.uniforms.u_aspect.value = this.uniforms.u_resolution.value.x / this.uniforms.u_resolution.value.y;
    return composer.render(this.material, toScreen);
  }

}

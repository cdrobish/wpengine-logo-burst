import settings from 'core/settings';
import Simulator from './simulator';
import {
  positionRenderTarget,
  prevPositionRenderTarget,
  initAnimation,
  textureDefaultColor
} from './simulator';
import {
  createTriangleMesh,
} from 'mesh/';
import {
  distanceMaterial,
  motionMaterial,
  triangleColorImageShader,
  triangleShader
} from 'materials';

const toRadian =  Math.PI/180;

const SHADER_TYPE_COLORS = 'colors';
const SHADER_TYPE_IMAGE = 'image';

let mesh;

class Particles {
  constructor() {}

  init = (type) => {
    mesh = createTriangleMesh();
    mesh.position.y = 100;
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    console.log(type, settings.particle.baseColor, settings.particle.fadeColor)

    if(type === SHADER_TYPE_IMAGE) {
      mesh.material = triangleColorImageShader;
      mesh.material.uniforms.textureDefaultColor.value = textureDefaultColor;
    } else {
      mesh.material = triangleShader;
      mesh.material.uniforms.color1.value = new THREE.Color(settings.particle.baseColor);
      mesh.material.uniforms.color2.value = new THREE.Color(settings.particle.fadeColor);
    }
    mesh.material.uniforms.cameraMatrix.value = settings.camera.matrixWorld;
    mesh.customDistanceMaterial = distanceMaterial;
    mesh.motionMaterial = motionMaterial;
    return mesh;
  }

  update = (delta) => {

    mesh.material.uniforms.texturePosition.value = positionRenderTarget;
    mesh.customDistanceMaterial.uniforms.texturePosition.value = positionRenderTarget;
    mesh.motionMaterial.uniforms.texturePrevPosition.value = prevPositionRenderTarget;

    if(mesh.material.uniforms.flipRatio ) {
        mesh.material.uniforms.flipRatio.value ^= 1;
        mesh.customDistanceMaterial.uniforms.flipRatio.value ^= 1;
        mesh.motionMaterial.uniforms.flipRatio.value ^= 1;
    }

    let {
      positionX,
      positionY,
      positionZ,
      rotationX,
      rotationY,
      rotationZ
    } = settings.emitter;

    mesh.position.set(positionX, positionY, positionZ);
    mesh.rotation.set(rotationX * toRadian, rotationY * toRadian, rotationZ * toRadian);
  }
}

let particles = new Particles(SHADER_TYPE_COLORS);

const updateParticles = (type) => {
  let {parent} = mesh;
  mesh.parent.remove(mesh);
  //mesh = this.init(type);
  mesh = this.c.init(type);
  parent.add(mesh);
}
export default particles;
export {
  mesh,
  updateParticles,
  SHADER_TYPE_COLORS,
  SHADER_TYPE_IMAGE
}

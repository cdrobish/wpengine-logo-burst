import _ from 'lodash';

const getEmitterDimensions = (geometry, multiplier) => {
  geometry.computeBoundingBox();
  multiplier = multiplier || 1;
  let emitterWidth = (Math.abs(geometry.boundingBox.min.x) + Math.abs(geometry.boundingBox.max.x)) * multiplier;
  let emitterHeight = (Math.abs(geometry.boundingBox.min.y) + Math.abs(geometry.boundingBox.max.y)) * multiplier;
  return {
    width:emitterWidth,
    height:emitterHeight,
  }
}

const distanceCompare = (a, b) => {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
}

const locationOf = (element, array, comparer, start, end) => {
    if (array.length === 0)
        return -1;

    start = start || 0;
    end = end || array.length;
    let pivot = (start + end) >> 1;  // should be faster than dividing by 2

    let c = comparer(element, array[pivot]);
    if (end - start <= 1) return c == -1 ? pivot - 1 : pivot;

    switch (c) {
        case -1: return locationOf(element, array, comparer, start, pivot);
        case 0: return pivot;
        case 1: return locationOf(element, array, comparer, pivot, end);
    };
};

const addPositionData = (multiDataArr, i4, options) => {

  // multiDataArr = [positions, &| colors]
  let {
     x,
     y,
     z,
     life,
     emitterWidth,
     emitterHeight,
     colorMap
  } = options;

  multiDataArr[0][i4 + 0] = x;
  multiDataArr[0][i4 + 1] = y;
  multiDataArr[0][i4 + 2] = z;
  multiDataArr[0][i4 + 3] = life;
  let r = 0, g = 0, b = 0, a = 0;
  if(multiDataArr.length > 1){
    // width / height for sphere can be - 50 to 50
    let relX = ~~(((x + emitterWidth/2) / emitterWidth) * colorMap.width);
    let relY = ~~(((y + emitterHeight/2) / emitterHeight) * colorMap.height);
    let colorIndex = (relY * colorMap.width * 4) - ((colorMap.width - relX) * 4);

    if(colorIndex + 4 > 0 && colorIndex + 4 < colorMap.data.length){
      r = colorMap.data[colorIndex];
      g = colorMap.data[colorIndex + 1];
      b = colorMap.data[colorIndex + 2];
      a = colorMap.data[colorIndex + 3];
    }

    multiDataArr[1][i4 + 0] = r/255;
    multiDataArr[1][i4 + 1] = g/255;
    multiDataArr[1][i4 + 2] = b/255;
    multiDataArr[1][i4 + 3] = a/255;
  }
}

const createTextures = (options) => {
  let {
    positions,
    colors,
    textureWidth,
    textureHeight
  } = options;

  let positionTexture,
    colorTexture;



  const createTexture = (values) => {
    let texture = new THREE.DataTexture(values, textureWidth, textureHeight, THREE.RGBAFormat, THREE.FloatType);
    texture.minFilter = THREE.NearestFilter;
    texture.magFilter = THREE.NearestFilter;
    texture.needsUpdate = true;
    texture.generateMipmaps = false;
    texture.flipY = true;
    return texture;
  }

  if(positions){
     positionTexture = createTexture(positions);
  }

  if(colors){
    colorTexture = createTexture(colors);
  }

  return {
    positionTexture,
    colorTexture
  }

}

const getVertexData = (options) => {

  let {
    geometry,
    colorMap,
    multiplier,
    amount
  } = options;

  let edges = new THREE.EdgesGeometry( geometry );
  let edgesVertices = edges.getAttribute('position').array;
  let positions = new Float32Array(amount * 4);
  let colors = colorMap ? new Float32Array(amount * 4) : null;
  let i4;
  let randomVertexIndex;
  let x, y, z, life;
  let dimensions = getEmitterDimensions(geometry, multiplier);

  for (let i = 0; i < amount; i++) {
   i4 = i * 4;
   randomVertexIndex = 3 * (~~(Math.random() * (edgesVertices.length / 3)));
   x = multiplier * edgesVertices[randomVertexIndex + 0];
   y = multiplier * edgesVertices[randomVertexIndex + 1];
   z = multiplier * edgesVertices[randomVertexIndex + 2];
   life = Math.random();
   addPositionData(_.compact([positions, colors]), i4, {
     x,
     y,
     z,
     life,
     colorMap,
     emitterWidth:dimensions.width,
     emitterHeight:dimensions.height
   });
  }
  return {
    positions,
    colors
  }
}

const getEdgeData = (options) => {
  let {
    geometry,
    colorMap,
    amount,
    multiplier
  } = options;

  let edges = new THREE.EdgesGeometry( geometry );
  let edgesVertices = edges.getAttribute('position').array;
  let numEdges = edgesVertices.length / 3 / 2;
  let edgesLines = [];
  let i, i6;
  let dimensions = getEmitterDimensions( geometry, multiplier );

  for (i = 0; i < numEdges; i++)
  {
    i6 = i * 6;
    edgesLines.push(new THREE.Line3(
      new THREE.Vector3(
        edgesVertices[i6 + 0],
        edgesVertices[i6 + 1],
        edgesVertices[i6 + 2]
      ).multiplyScalar(multiplier),
      new THREE.Vector3(
        edgesVertices[i6 + 3],
        edgesVertices[i6 + 4],
        edgesVertices[i6 + 5]
      ).multiplyScalar(multiplier)
    ));
  }

  let totalDistance = 0;
  let edgesDistancesTally = [];
  let distance;
  for (i = 0; i < numEdges; i++)
  {
    distance = edgesLines[i].distance();
    totalDistance += distance;
    edgesDistancesTally.push(totalDistance);
  }
  let positions = new Float32Array(amount * 4);
  let colors = colorMap ? new Float32Array(amount * 4) : null;
  let x, y, z, life;

  let i4;
  let randomEdgeDistance, randomEdgeLength, randomSegmentLength, previousEdgesDistanceTally, segmentPercentage;
  let indexOfEdgeAtRandomDistance;
  let randomPositionOnEdge;

  for (i = 0; i < amount; i++) {
    i4 = i * 4;
    randomEdgeDistance = Math.random() * totalDistance;
    indexOfEdgeAtRandomDistance = 1 + locationOf(randomEdgeDistance, edgesDistancesTally, distanceCompare);
    previousEdgesDistanceTally = (indexOfEdgeAtRandomDistance > 0) ? edgesDistancesTally[indexOfEdgeAtRandomDistance - 1] : 0;
    randomSegmentLength = randomEdgeDistance - previousEdgesDistanceTally;
    randomEdgeLength = edgesDistancesTally[indexOfEdgeAtRandomDistance] - previousEdgesDistanceTally
    segmentPercentage = randomSegmentLength / randomEdgeLength;
    randomPositionOnEdge = edgesLines[indexOfEdgeAtRandomDistance].at(segmentPercentage);
    let {
      x,y,z
    } = randomPositionOnEdge;
    life = Math.random();
    addPositionData(_.compact([positions, colors]), i4, {
      x,
      y,
      z,
      life,
      colorMap,
      emitterWidth:dimensions.width,
      emitterHeight:dimensions.height
    });
  }
  return {
    positions,
    colors
  }
}

const createFromGeometry = (options) => {

  let {
    geometry,
    mode,
    colorMap,
    textureWidth,
    textureHeight
  } = options;

  let {
    positions,
    colors
  } = (mode && mode === MODE_VERTICES) ? getVertexData(options) : getEdgeData(options);

  return createTextures({
    positions,
    colors,
    textureWidth,
    textureHeight
  });

}

const createFromValues = (options) => {
  let {
    amount,
    values,
    colorMap,
    width,
    height,
    multiplier,
    textureWidth,
    textureHeight
  } = options;

  let positions = new Float32Array(amount * 4);
  let colors = colorMap ? new Float32Array(amount * 4) : null;
  let x, y, z, life;
  for (let i4 = 0; i4 < values.length; i4 += 4) {
    x = values[i4 + 0];
    y = values[i4 + 1];
    z = values[i4 + 2];
    life = values[i4 + 4];
    addPositionData(_.compact([positions, colors]), i4, {
      x,
      y,
      z,
      life,
      colorMap,
      emitterWidth:width,
      emitterHeight:height
    });
  }
  return createTextures({
    positions,
    colors,
    textureWidth,
    textureHeight
  });
}

const TYPE_GEOMETRY = 'geom';
const TYPE_VALUES = 'values';
const MODE_EDGES = 'edges';
const MODE_VERTICES = 'vertices';

const Emitter = {
  create : (options) => {

    let {
      type,
      geometry,
    } = options;

    if(type && type === TYPE_GEOMETRY) {
      if(geometry){
        return createFromGeometry(options);
      } else {
        console.warn('No geometry has been passed through {options}, with type as geom');
      }
    } else if(type && type === TYPE_VALUES) {
      return createFromValues(options);
    }
  }
}

export default Emitter;

export {
  TYPE_GEOMETRY,
  TYPE_VALUES,
  MODE_EDGES,
  MODE_VERTICES
}

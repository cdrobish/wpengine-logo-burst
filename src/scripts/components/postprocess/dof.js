
const undef;

export default class DOF extends Effect {
  constructor(){

    super({
      name:'dof',
      uniforms: {
          u_distance: { type: 't', value: undef },
          u_dofDistance: { type: 'f', value: 0 },
          u_delta: { type: 'v2', value: new THREE.Vector2() },
          u_mouse: { type: 'v2', value: settings.mouse },
          u_amount: { type: 'f', value: 1 }
      },
      fragmentShader: glslify('./dof.frag')
    );
    console.log(this)
  }
  render(delta, target, toScreen) {
    // console.log(bloomBlurShader)

    let {
      enabled,
      blurX,
      blurY,
      amount
    } = settings.bloom;

    let tmpTarget1 = composer.getTarget(BLUR_BIT_SHIFT);
    let tmpTarget2 = composer.getTarget(BLUR_BIT_SHIFT);

    composer.releaseTarget(tmpTarget1, tmpTarget2);

    bloomBlurShader.uniforms.u_texture.value = target;
    bloomBlurShader.uniforms.u_delta.value.set(blurX / resolution.x, 0);
    fbo.render(bloomBlurShader, tmpTarget1);

    bloomBlurShader.uniforms.u_texture.value = tmpTarget1;
    bloomBlurShader.uniforms.u_delta.value.set(0, blurY / resolution.y);
    fbo.render(bloomBlurShader, tmpTarget2);

    this.uniforms.u_blurTexture.value = tmpTarget2;
    this.uniforms.u_amount.value = amount;

    super.render.call(this, delta, target, toScreen);
  }
}

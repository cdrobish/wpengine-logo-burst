import fbo from './fbo';
import composer from './composer';

import Fxaa from './fxaa';
import MotionBlur from './motionBlur';
import Bloom from './bloom';

import {
  camera,
  renderer,
  scene
} from 'components/view';

import {
  copy
} from 'components/postprocess/fbo';

let fxaa,
  motionBlue,
  bloom,
  visualizeTarget;

export default class Postprocess {
  constructor(){
    composer.queue.push(new Fxaa());
    composer.queue.push(new MotionBlur());
    composer.queue.push(new Bloom());
  }
  resize = (width, height) => {
    composer.resize(width,height);
  }
  render = (delta) => {
    composer.renderQueue(delta);
    if(visualizeTarget){
      copy(visualizeTarget);
    }
  }
}

export { visualizeTarget }

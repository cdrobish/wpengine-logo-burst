varying vec4 vColor;
varying float vLife;

uniform bool useImage;
uniform vec3 color1;
uniform vec3 color2;


void main() {
    vec3 outgoingLight;
    if(useImage) {
      outgoingLight = vColor.rgb;
    } else {
      outgoingLight = mix(color2, color1, smoothstep(0.0, 0.7, vLife));
    }
    gl_FragColor = vec4( outgoingLight, 0.0 );
}

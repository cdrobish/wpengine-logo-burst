import _ from 'lodash';
import {
  camera,
  scene,
  renderer
} from 'components/view';

import merge from 'mout/object/merge'
import fbo from 'components/postprocess/fbo';

import {
  createTarget,
  render
} from 'components/postprocess/fbo';

let undef;

let fromTarget,
 toTarget,
 targetCounts = {},
 targetLists = {},
 targetDefaultState = {
    depthBuffer : false,
    texture: {
        generateMipmaps : false
    }
 },
 resolution = new THREE.Vector2()

const filterQueue = (effect) => {
  return effect.enabled;
};

const getTargetList = (id) => {
  let list = targetLists[id];
  if(!list){
    targetLists[id] = [];
  }
  return targetLists[id];
}

class Composer {
  constructor(){
    fromTarget = createTarget();
    toTarget = createTarget();
    this.queue = [];
  }

  resize = (width, height) => {
    resolution.set(width, height);
    fromTarget.setSize(width,height);
    toTarget.setSize(width,height);
    _.each(this.queue, (effect) => {
      effect.resize(width,height);
    });
  }

  renderQueue = (delta) => {
    let filteredQueue = this.queue.filter(filterQueue);
    let length = filteredQueue.length;
    // console.log(filteredQueue)
    if(length) {
      toTarget.depthBuffer = true;
      toTarget.stencilBuffer = true;
      renderer.render( scene, camera, toTarget );
      this.swapTarget();

      _.each(filteredQueue, (effect, index) => {
        effect.render(delta, fromTarget, index === length - 1);
      });

    } else {
      renderer.render(scene,camera);
    }
  }

  renderScene = (target, targetScene, targetCamera) => {
    let renderScene = targetScene || scene;
    let renderCamera =  targetCamera || camera;
    if(target){
      renderer.render(renderScene, renderCamera, target);
    } else {
      renderer.render(renderScene, renderCamera)
    }
  }

  render = (material, toScreen) => {
    fbo.render(material, toScreen ? null : toTarget);
    this.swapTarget();
    return fromTarget;
  }

  swapTarget = () => {
    let tmp = toTarget;
    toTarget = fromTarget;
    fromTarget = tmp;
  }

  getTarget = (bitShift, isRGBA) => {
    bitShift = bitShift || 0;
    isRGBA = +(isRGBA || 0);

    let width = resolution.x >> bitShift,
      height = resolution.y >> bitShift,
      id = bitShift + '_' + isRGBA,
      list = getTargetList(id),
      target;

    if(list.length){
      target = list.pop();
      merge(target, targetDefaultState);
    } else {
      target = createTarget(width,height,isRGBA ? THREE.RGBAFormat : THREE.RGBFormat);
      target._listId = id;
      targetCounts[id] = targetCounts[id] || 0;
    }

    targetCounts[id]++;

    if(target.width !== width || target.height !== height){
      renderer.setSize(width,height);
    }
    return target;
  }

  releaseTarget = (...targets) => {
    let len = targets.length,
      i = 0,
      j = 0,
      target,
      found,
      jlen,
      id,
      list;
    for(i; i < len; i++){
      target = targets[i];
      id = target._listId;
      list = getTargetList(id);
      found = false;
      targetCounts[id]--;
      jlen = list.length;
      for(j = 0; j < jlen; j++){
        if(list[j] === target){
          found = true;
          break;
        }
      }
      if(!found){
        list.push(target);
      }
    }
  }

}

const composer = new Composer();

export default composer;

export {
  resolution,
  fromTarget,
  toTarget
}

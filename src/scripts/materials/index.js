import settings from 'core/settings';

import {
  prefix,
  renderer
} from 'components/view';

import particlesVert from 'glsl/particles.vert';
// import particlesFrag from 'glsl/particles.frag';
import particlesImageColorFrag from 'glsl/particleImageColor.frag';
import particlesColorFrag from 'glsl/particleColor.frag';
import particlesDistanceFrag from 'glsl/particlesDistance.frag';

import quadVert from 'glsl/quad.vert';
import postQuadVert from 'glsl/postQuad.vert';
import postQuadFrag from 'glsl/postQuad.frag';
import positionFrag from 'glsl/position.frag';

import velocityFrag from 'glsl/velocity.frag';

import distanceVert from 'glsl/particlesDistance.vert';
import distanceFrag from 'glsl/particlesDistance.frag';

import throughFrag from 'glsl/through.frag';
import trianglesVert from 'glsl/triangles.vert';
import trianglesImageColorVert from 'glsl/trianglesImageColor.vert';
import trianglesMotionVert from 'glsl/trianglesMotion.vert';
import trianglesDistanceVert from 'glsl/trianglesDistance.vert';

import fbo from 'components/postprocess/fbo';
import composer from 'components/postprocess/composer';
import bloomBlurFrag from 'glsl/bloomBlur.frag';

import motionBlurLinesVert from 'glsl/motionBlurLines.vert';
import motionBlurLinesFrag from 'glsl/motionBlurLines.frag';
import motionBlurSamplingFrag from 'glsl/motionBlurSampling.frag';

import MeshMotionMaterial from './MeshMotionMaterial';

let undef;

let {
  width,
  height
} = settings.getSize();

const prefixStr = prefix || 'precision mediump float;\n';
const sampleCount = '#define SAMPLE_COUNT ' + (settings.motionBlur.sampleCount || 21) + '\n';

const copyPostRawShader = new THREE.RawShaderMaterial({
  uniforms: {
      u_texture: { type: 't', value: undef }
  },
  vertexShader: prefixStr + postQuadVert,
  fragmentShader: prefixStr + postQuadFrag
});

const copyRawShader = new THREE.RawShaderMaterial({
  uniforms: {
    resolution: {
      type: 'v2',
      value: new THREE.Vector2(width, height)
    },
    texture: {
      type: 't',
      value: undef
    }
  },
  vertexShader: prefixStr + quadVert,
  fragmentShader: prefixStr + throughFrag
});

const particleShader = new THREE.ShaderMaterial({
  uniforms: THREE.UniformsUtils.merge([
  THREE.UniformsLib.shadowmap,
    {
      texturePosition: {
        type: 't',
        value: undef
      },
      textureDefaultColor : {
        type: 't',
        value: undef
      },
      color1: {
        type: 'c',
        value: undef
      },
      color2: {
        type: 'c',
        value: undef
      }
}]),
  vertexShader: prefixStr + particlesVert,
  fragmentShader: prefixStr + particlesColorFrag,
  blending: THREE.NoBlending
});

const velocityRawShader = new THREE.RawShaderMaterial({
  uniforms: {
    resolution: {
      type: 'v2',
      value: new THREE.Vector2(width, height)
    },
    texturePosition: {
      type: 't',
      value: undef
    },
    textureVelocity: {
      type: 't',
      value: undef
    },
    timeEffect: {
      type: 'f',
      value: 1
    },
    vecSpeed: {
      type: 'v3',
      value: new THREE.Vector3
    },
    vecSpeedVar: {
      type: 'v3',
      value: new THREE.Vector3
    },
    gravity: {
      type: 'f',
      value: 0
    },
    dieSpeed: {
      type: 'f',
      value: 0
    },
    time: {
      type: 'f',
      value: 0
    }
  },
  vertexShader: prefixStr + quadVert,
  fragmentShader: prefixStr + velocityFrag,
  blending: THREE.NoBlending,
  transparent: false,
  depthWrite: false,
  depthTest: false
});

const positionRawShader = new THREE.RawShaderMaterial({
  uniforms: {
    resolution: {
      type: 'v2',
      value: new THREE.Vector2(width, height)
    },
    texturePosition: {
      type: 't',
      value: undef
    },
    textureDefaultPosition: {
      type: 't',
      value: undef
    },
    textureVelocity: {
      type: 't',
      value: undef
    },
    mouse3d: {
      type: 'v3',
      value: new THREE.Vector3
    },
    timeEffect: {
      type: 'f',
      value: 1
    },
    speed: {
      type: 'f',
      value: 1
    },
    dieSpeed: {
      type: 'f',
      value: 0
    },
    emitterSize: {
      type: 'f',
      value: 0
    },
    curlSize: {
      type: 'f',
      value: 0
    },
    curlStrength: {
      type: 'f',
      value: 0
    },
    attractionFactor: {
      type: 'f',
      value: 0
    },
    attractionStrength: {
      type: 'f',
      value: 0
    },
    time: {
      type: 'f',
      value: 0
    },
    initAnimation: {
      type: 'f',
      value: 0
    }
  },
  vertexShader: prefixStr + quadVert,
  fragmentShader: prefixStr + positionFrag,
  blending: THREE.NoBlending,
  transparent: false,
  depthWrite: false,
  depthTest: false
});

const triangleColorImageShader = new THREE.ShaderMaterial({
  uniforms: THREE.UniformsUtils.merge([
    THREE.UniformsLib.shadowmap,
    {
      resolution: {
        type: 'v2',
        value: new THREE.Vector2(width, height)
      },
      texturePosition: {
        type: 't',
        value: undef
      },
      flipRatio: {
        type: 'f',
        value: 0
      },
      textureDefaultColor : {
        type: 't',
        value: undef
      },
      cameraMatrix: {
        type: 'm4',
        value: undef
      }
          }
      ]),
  vertexShader: prefixStr + trianglesImageColorVert,
  fragmentShader: prefixStr + particlesImageColorFrag,
  blending: THREE.NoBlending
});

const triangleShader = new THREE.ShaderMaterial({
  uniforms: THREE.UniformsUtils.merge([
    THREE.UniformsLib.shadowmap,
    {
      resolution: {
        type: 'v2',
        value: new THREE.Vector2(width, height)
      },
      texturePosition: {
        type: 't',
        value: undef
      },
      flipRatio: {
        type: 'f',
        value: 0
      },
      color1: {
        type: 'c',
        value: undef
      },
      color2: {
        type: 'c',
        value: undef
      },
      cameraMatrix: {
        type: 'm4',
        value: undef
      }
    }]),
  vertexShader: prefixStr + trianglesVert,
  fragmentShader: prefixStr + particlesColorFrag,
  blending: THREE.NoBlending
});

const distanceShader = new THREE.ShaderMaterial({
  uniforms: {
      lightPos: { type: 'v3', value: new THREE.Vector3( 0, 0, 0 ) },
      texturePosition: { type: 't', value: undef }
  },
  vertexShader: prefixStr + distanceVert,
  fragmentShader: prefixStr + distanceFrag,
  depthTest: true,
  depthWrite: true,
  side: THREE.BackSide,
  blending: THREE.NoBlending
});

const bloomBlurShader = new THREE.RawShaderMaterial({
    uniforms: {
        u_texture: { type: 't', value: undef },
        u_delta: { type: 'v2', value: new THREE.Vector2() }
    },
    vertexShader: fbo.vertexShader,
    fragmentShader: prefixStr + bloomBlurFrag
});

// Motion Blur
const linesMaterial = new THREE.RawShaderMaterial({
    uniforms: {
        u_texture: { type: 't', value: undef },
        u_maxDistance: { type: 'f', value: 1 },
        u_jitter: { type: 'f', value: 0.3 },
        u_fadeStrength: { type: 'f', value: 1 },
        u_motionMultiplier: { type: 'f', value: 1 },
        u_depthTest: { type: 'f', value: 0 },
        u_opacity: { type: 'f', value: 1 },
        u_leaning: { type: 'f', value: 0.5 },
        u_depthBias: { type: 'f', value: 0.01 }
    },
    vertexShader: prefixStr + motionBlurLinesVert,
    fragmentShader: prefixStr + motionBlurLinesFrag,
    blending : THREE.CustomBlending,
    blendEquation : THREE.AddEquation,
    blendSrc : THREE.OneFactor,
    blendDst : THREE.OneFactor ,
    blendEquationAlpha : THREE.AddEquation,
    blendSrcAlpha : THREE.OneFactor,
    blendDstAlpha : THREE.OneFactor,
    depthTest: false,
    depthWrite: false,
    transparent: true
});

const samplingMaterial = new THREE.RawShaderMaterial({
  uniforms: {
      u_texture: { type: 't', value: undef },
      u_maxDistance: { type: 'f', value: 1 },
      u_fadeStrength: { type: 'f', value: 1 },
      u_motionMultiplier: { type: 'f', value: 1 },
      u_leaning: { type: 'f', value: 0.5 }
  },
  defines: {
      SAMPLE_COUNT: sampleCount || 21
  },
  fragmentShader: prefixStr + sampleCount + motionBlurSamplingFrag
});

const distanceMaterial = new THREE.ShaderMaterial( {
  uniforms: {
    lightPos: { type: 'v3', value: new THREE.Vector3( 0, 0, 0 ) },
    texturePosition: { type: 't', value: undef },
    flipRatio: { type: 'f', value: 0 }
  },
  vertexShader: trianglesDistanceVert,
  fragmentShader: particlesDistanceFrag,
  depthTest: true,
  depthWrite: true,
  side: THREE.BackSide,
  blending: THREE.NoBlending
});

const motionMaterial = new MeshMotionMaterial({
  uniforms: {
    texturePosition: { type: 't', value: undef },
    texturePrevPosition: { type: 't', value: undef },
    flipRatio: { type: 'f', value: 0 }
  },
  vertexShader: trianglesMotionVert,
  depthTest: true,
  depthWrite: true,
  side: THREE.DoubleSide,
  blending: THREE.NoBlending
})

export {
  distanceMaterial,
  trianglesDistanceVert,
  motionMaterial,
  bloomBlurShader,
  copyRawShader,
  copyPostRawShader,
  particleShader,
  positionRawShader,
  velocityRawShader,
  triangleShader,
  triangleColorImageShader,
  distanceShader,
  linesMaterial,
  samplingMaterial
}

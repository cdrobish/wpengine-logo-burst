import {TweenMax, TimelineMax, Quad, Expo, Cubic} from 'gsap';
import settings from 'core/settings';

const timeline = () => {
  let ease = Cubic.easeInOut,
    explodeTime = 0.25,
    scaleTime = 0.4;

  const tl = new TimelineMax({});

  // timeline for showing emitter;
  let resolveToEmitter = new TimelineMax();
  resolveToEmitter.to(settings.behavior, 0.3, {
    curlSize:0,
    curlStrength:0,
    dieSpeed:0.05,
    attractionFactor:0,
    attractionStrength:0,
    ease:Expo.easeIn,
  }, 'resolve');

  resolveToEmitter.to(settings.rendering, 0.4, {
    timeScale:1,
    dieSpeed:0.02,
    ease:Expo.easInOut
  }, 'resolve');

  let implode = new TimelineMax();
  implode.to(settings.behavior, 0.4, {
    curlSize:0.0435,
    curlStrength:1,
    attractionFactor:4,
    attractionStrength:10,
    dieSpeed:0.05,
    ease:Expo.easeInOut,
    delay:0.2
  }, 'implode');

  implode.to(settings.emitter, 0.4, {
    size:0.25,
    ease:Expo.easeInOut

  }, 'implode');

  implode.to(settings.motionBlur, 0.6, {
    maxDistance:100,
    motionMultiplier:80,
    ease:Expo.easeInOut,
  }, 'implode');

  implode.to(settings.motionBlur, 0.6, {
    maxDistance:30,
    motionMultiplier:10,
    ease:Expo.easeInOut,
  }, 'implode+=0.6');

  implode.to(settings.rendering, 0.6, {
    timeScale:0.3,
    ease:Expo.easeInOut,
  }, 'implode+=0.4');

  let bloom = new TimelineMax();
  bloom.to(settings.bloom, 0.6, {amount:3.25, ease:Cubic.easeOut});
  bloom.to(settings.bloom, 1, {amount:2, ease:Cubic.easeOut});

  let explode = new TimelineMax();
  explode.to(settings.emitter, explodeTime, {
    size:0.5,
    positionZ:0,
    ease:Expo.easeOut,
  }, 'explode');

  explode.to(settings.behavior, explodeTime, {
    attractionFactor:-2,
    attractionStrength:33,
    vecSpeedZ:20,
    dieSpeed:0.02,
    curlStrength:68,
    curlSize:0.004,

    ease:Expo.easeOut,
  }, 'explode');

  explode.to(settings.rendering, 0.6, {timeScale:0.0001}, '-=0.4');

  tl.add(resolveToEmitter);
  tl.add(implode);
  tl.add(bloom, '-=1');
  tl.add(explode, '-=1.2');


  return tl;
}

export default timeline;

import {
  renderer,
  prefix
} from 'components/view';

import {
  copyPostRawShader
} from 'materials'

import quadVert from 'glsl/postQuad.vert';

const scene = new THREE.Scene();
const camera = new THREE.Camera();

let mesh;

const createTarget = (width, height, format, type, minFilter, magFilter) => {

  format = format || THREE.RGBFomrat;
  type = type || THREE.UnsignedByteType;
  minFilter = minFilter || THREE.LinearFilter;
  magFilter = magFilter || THREE.LinearFilter;

  let target = new THREE.WebGLRenderTarget(width  || 1, height || 1, {
    format,
    type,
    minFilter,
    magFilter
  });
  target.texture.generateMipMaps = false;
  return target;
};

const getColorState = () => {

  let {autoClearColor} = renderer,
  clearColor = renderer.getClearColor().getHex(),
  clearAlpha = renderer.getClearAlpha()

  return {
    autoClearColor,
    clearColor,
    clearAlpha
  };
};

const setColorState = (state) => {
  let {
    clearColor,
    clearAlpha,
    autoClearColor
  } = state;

  renderer.setClearColor(clearColor, clearAlpha);
  renderer.autoClearColor = autoClearColor;
}

const copy = (input, output) => {
  mesh.material = copyPostRawShader;
  copyPostRawShader.uniforms.u_texture.value = input;
  if(output){
    renderer.render(scene, camera, output);
  } else {
    renderer.render(scene, camera);
  }
}

class FBO {

  constructor(){
    camera.position.z = 1;
    mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(2,2), copyPostRawShader);
    this.vertexShader = quadVert;
    scene.add(mesh);
  }

  render = (material, target) => {
    mesh.material = material;
    if(target){
      renderer.render(scene,camera,target);
    } else {
      renderer.render(scene,camera);
    }
  }

}

const fbo = new FBO();

export default fbo;

export {
  copy,
  render,
  mesh,
  createTarget,
  getColorState,
  setColorState
}

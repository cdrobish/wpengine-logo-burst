import _ from 'lodash';
import settings from 'core/settings';
import {TweenMax, TimelineMax} from 'gsap';

const randomWind = () => {

  let { behavior } = settings;
  let numberOfSteps = 10;
  let steps = [];

  const setRandomValues = () => {
    for(let i = 0; i < numberOfSteps; i++){
      steps.push({
        vecSpeedX : _.random(-30, 30),
        vecSpeedY : _.random(-30, 30),
        vecSpeedZ : _.random(-30, 30)
      })
    }
  }
  setRandomValues();

  const onComplete = () => {
    console.log('completed');
    setRandomValues();
  }

  const onUpdate = () => {
    if(tl.paused && behavior.vecSpeedX !== 0){
      TweenMax.to(behavior, 0.5, {vecSpeedX:0, vecSpeedY:0, vecSpeedZ:0});
    }
  }

  let tl = new TimelineMax({
    onUpdate,
    onComplete
  });


  _.each(steps, (step) => {
    console.log(step)
    let {
      vecSpeedX,
      vecSpeedY,
      vecSpeedZ
      } = step;
      tl.to(behavior, _.random(0.5, 2.5), {vecSpeedX, vecSpeedY, vecSpeedZ});
  });
  console.log('tl',tl);
  return tl;

}


export {
  randomWind
}

import lowFxaaVert from 'glsl/lowFxaa.vert';
import lowFxaaFrag from 'glsl/lowFxaa.frag';
import fxaaFrag from 'glsl/fxaa.frag';
import Effect from './effect';
import settings from 'core/settings';

import { prefix } from 'components/view';
const prefixStr = prefix || 'precision mediump float;\n';

export default class Fxaa extends Effect {
  constructor(){
    let vertexShader = settings.fxaa.quality === 'low' ? lowFxaaVert : undefined,
      fragmentShader = settings.fxaa.quality === 'low' ? lowFxaaFrag : fxaaFrag;
    super({
      name:'fxaa',
      uniforms: {},
      vertexShader,
      fragmentShader
    });
  }
}

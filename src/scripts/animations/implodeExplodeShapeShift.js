import {TweenMax, TimelineMax, Quad, Expo, Cubic} from 'gsap';
import settings from 'core/settings';

const timeline = (gui) => {

  let {
    logo,
    emitter,
    behavior,
    bloom,
    motionBlur,
    rendering
  } = settings;

  let emitterTypeCtrl;
  for (let i in gui.__folders) {
    for (let j in gui.__folders[i].__controllers) {
      let prop = gui.__folders[i].__controllers[j].property;
      if(prop === 'type') {
        emitterTypeCtrl = gui.__folders[i].__controllers[j];
      }
    }
  }

  let ease = Cubic.easeInOut,
    explodeTime = 0.25,
    scaleTime = 0.4;



  // timeline for showing emitter;
  // let resolveToEmitter = new TimelineMax();
  // resolveToEmitter.to(behavior, 0.3, {
  //   curlSize:0,
  //   curlStrength:0,
  //   dieSpeed:0.05,
  //   attractionFactor:0,
  //   attractionStrength:0,
  //   ease:Expo.easeIn,
  // }, 'resolve');
  //
  // resolveToEmitter.to(rendering, 0.4, {
  //   timeScale:1,
  //   dieSpeed:0.02,
  //   ease:Expo.easInOut
  // }, 'resolve');

  let implode = new TimelineMax();
  implode.to(behavior, 0.4, {
    curlSize:0.0435,
    curlStrength:1,
    attractionFactor:4,
    attractionStrength:10,
    dieSpeed:0.05,
    ease:Expo.easeInOut,
    delay:0.1
  }, 'implode');

  implode.to(emitter, 0.4, {
    size:0.25,
    ease:Expo.easeInOut
  }, 'implode');

  implode.to(motionBlur, 0.6, {
    maxDistance:100,
    motionMultiplier:80,
    ease:Expo.easeInOut,
  }, 'implode');

  implode.to(motionBlur, 0.6, {
    maxDistance:30,
    motionMultiplier:10,
    ease:Expo.easeInOut,
  }, 'implode+=0.6');

  implode.to(rendering, 0.6, {
    timeScale:0.3,
    ease:Expo.easeInOut,
  }, 'implode+=0.4');

  // implode.timeScale(0.9);

  let bloomTL = new TimelineMax();
  bloomTL.to(bloom, 0.6, {amount:3.25, ease:Cubic.easeOut});
  bloomTL.to(bloom, 1, {amount:2, ease:Cubic.easeOut});

  let explode = new TimelineMax();
  explode.to(emitter, explodeTime, {
    size:0.5,
    positionZ:0,
    ease:Expo.easeOut,
  }, 'explode');

  explode.to(behavior, explodeTime, {
    attractionFactor:-2,
    attractionStrength:33,
    vecSpeedZ:20,
    dieSpeed:0.02,
    curlStrength:68,
    curlSize:0.004,
    ease:Expo.easeOut,
  }, 'explode');

  explode.to(rendering, 0.6, {timeScale:0.0001}, '-=0.4');

  let emitterType = 'Logo Outline';

  let changeEmiiter = () => {
    let newType = emitterType === 'Logo Outline' ? 'Geo Sphere' : 'Logo Outline';
    emitterTypeCtrl.setValue(newType)
    emitterType = newType;
  }

  // tl.add(resolveToEmitter);


  let resolveToEmitter = new TimelineMax({onStart:() => {
    console.log('started')
  }});
  console.log(emitter.size)
  resolveToEmitter.to(emitter, 0.5, {
    size:12,
    ease:Cubic.easeInOut
  }, 'resolve');
  resolveToEmitter.to(rendering, 0.5, {
    timeScale:0.8,
    ease:Cubic.easeInOut
  }, 'resolve');
  resolveToEmitter.to(behavior, 0.5, {
    attractionFactor:0,
    attractionStrength:0,
    vecSpeedZ:0,
    dieSpeed:0.07,
    curlStrength:0.06,
    curlSize:0.0006,
    ease:Cubic.easeInOut
  }, 'resolve');


  // resolve.to(emitter, 5, {
  //   size:12
  // })
  const tl = new TimelineMax({});
  tl.add(resolveToEmitter);
  tl.add(implode, '+=2');
  tl.add(bloomTL, '-=1');
  tl.add(changeEmiiter, '-=1.2')
  tl.add(explode, '-=1.2');
  // tl.add(resolveToEmitter);



  return tl;
}

export default timeline;

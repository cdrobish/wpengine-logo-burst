import settings from 'core/settings';
import Emitter from './emitter';
import {
  TYPE_GEOMETRY,
  TYPE_VALUES,
  MODE_EDGES,
  MODE_VERTICES
} from './emitter';

import {
  renderer,
  logo
 } from './view';

import {
  copyRawShader,
  positionRawShader,
  velocityRawShader
} from 'materials/'

const {
  width,
  height
} = settings.getSize();

const amount = width * height;
const scene = new THREE.Scene();
const camera = new THREE.Camera();
const mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(2, 2), copyRawShader);

const getSphereData = () => {
  let i4, r, phi, theta;
  let positions = new Float32Array(amount * 4);
  for (let i = 0; i < amount; i++) {
    i4 = i * 4;
    r = (0.5 + Math.random() * 0.5) * 50;
    phi = (Math.random() - 0.5) * Math.PI;
    theta = Math.random() * Math.PI * 2;
    positions[i4 + 0] = r * Math.cos(theta) * Math.cos(phi); // x
    positions[i4 + 1] = r * Math.sin(phi); // y
    positions[i4 + 2] = r * Math.sin(theta) * Math.cos(phi); // z
    positions[i4 + 3] = Math.random(); // life
  }
  return positions;
}

const getPlaneData = () => {
  let i4;
  let positions = new Float32Array(amount * 4);
  for (let i = 0; i < amount; i++) {
    i4 = i * 4;
    positions[i4 + 0] = 50 - Math.random() * 100 // x
    positions[i4 + 1] = 50 - Math.random() * 100 // y
    positions[i4 + 2] = 0 // z
    positions[i4 + 3] = Math.random(); // life
  }
  return positions;
}


let initAnimation = 0;
let emitter,
    prevPositionRenderTarget,
    positionRenderTarget,
    positionRenderTarget2,
    textureDefaultColor,
    textureDefaultPosition,
    spherePosition,
    logoVertexPosition,
    logoEdgePosition,
    velocityRenderTarget,
    velocityRenderTarget2;

let colorMap;

export default class Simulator {

  constructor(options) {

    colorMap = options.colorMap || null;

    initAnimation = 1;
    camera.position.z = 1;
    scene.add(mesh);

    positionRenderTarget = new THREE.WebGLRenderTarget(width, height, {
      wrapS: THREE.ClampToEdgeWrapping,
      wrapT: THREE.ClampToEdgeWrapping,
      minFilter: THREE.NearestFilter,
      magFilter: THREE.NearestFilter,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      depthWrite: false,
      depthBuffer: false,
      stencilBuffer: false
    });
    positionRenderTarget2 = positionRenderTarget.clone();
    this.copyTexture(textureDefaultPosition, positionRenderTarget);
    this.copyTexture(positionRenderTarget, positionRenderTarget2);

    velocityRenderTarget = new THREE.WebGLRenderTarget(width, height, {
      wrapS: THREE.ClampToEdgeWrapping,
      wrapT: THREE.ClampToEdgeWrapping,
      minFilter: THREE.NearestFilter,
      magFilter: THREE.NearestFilter,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      depthWrite: false,
      depthBuffer: false,
      stencilBuffer: false
    });
    velocityRenderTarget2 = velocityRenderTarget.clone();
    this.copyTexture(this.createVelocityTexture(), velocityRenderTarget);
    this.copyTexture(velocityRenderTarget, velocityRenderTarget2);

    let type = settings.emitter.type || 'Sphere',
      mode = settings.emitter.mode || 'Edges';

    this.changeEmitterTo(type, mode);

  }


  updateColorMap = (map) => {
    colorMap = map;
  }

  copyTexture = (input, output) => {
    mesh.material = copyRawShader;
    copyRawShader.uniforms.texture.value = input;
    renderer.render(scene, camera, output);
  }

  changeEmitterTo = (type, mode) => {

    let values,
      multiplier,
      emitterWidth,
      emitterHeight,
      geometry;

    mode = mode === "Vertices" ? MODE_VERTICES : MODE_EDGES;

    if(type.toLowerCase() === 'sphere' || type.toLowerCase() === 'wall'){
      emitterWidth = 100;
      emitterHeight = 100;
      multiplier = 1;
      values = type.toLowerCase() === 'sphere' ? getSphereData() : getPlaneData();
      type = TYPE_VALUES;
    } else {


      let meshesMap = {
        'Logo' : logo.getObjectByName('logo'),
        'Logo Outline' : logo.getObjectByName('logo_outline'),
        'Logo Masked' : logo.getObjectByName('logo_masked'),
        'Geo Sphere' : new THREE.Mesh( new THREE.SphereGeometry( 0.005, 32, 32 ) ),
        'Cube' : new THREE.Mesh( new THREE.BoxGeometry( 0.05, 0.05, 0.05 ) )
      };

      geometry = (meshesMap[type] || meshesMap['Logo']).geometry;
      multiplier = 7000;
      type = TYPE_GEOMETRY;
    }

    let options = {
      amount,
      colorMap,
      type,
      mode,
      geometry,
      multiplier,
      width:emitterWidth,
      height:emitterHeight,
      textureWidth:width,
      textureHeight:height,
      values
    }

    let {
      positionTexture,
      colorTexture
    } =  Emitter.create(options);

    textureDefaultPosition = positionTexture;
    textureDefaultColor = colorTexture;
  }

  updateVelocity = (delta) => {
    let tmp = velocityRenderTarget;
    velocityRenderTarget = velocityRenderTarget2;
    velocityRenderTarget2 = tmp;
    mesh.material = velocityRawShader;
    velocityRawShader.uniforms.texturePosition.value = positionRenderTarget;
    velocityRawShader.uniforms.textureVelocity.value = velocityRenderTarget2;
    velocityRawShader.uniforms.time.value += delta * 0.001;
    renderer.render(scene, camera, velocityRenderTarget);
  }

  updatePosition = (delta) => {
    let tmp = positionRenderTarget;
    positionRenderTarget = positionRenderTarget2;
    positionRenderTarget2 = tmp;
    mesh.material = positionRawShader;
    positionRawShader.uniforms.textureDefaultPosition.value = textureDefaultPosition;
    positionRawShader.uniforms.texturePosition.value = positionRenderTarget2;
    positionRawShader.uniforms.textureVelocity.value = velocityRenderTarget;
    positionRawShader.uniforms.time.value += delta * 0.001;
    renderer.render(scene, camera, positionRenderTarget);
  }

  createVelocityTexture = () => {
    let velocities = new Float32Array(amount * 4);
    let i4;
    for (let i = 0; i < amount; i++) {
      i4 = i * 4;
      velocities[i4 + 0] = settings.behavior.vecSpeedX + settings.behavior.vecSpeedVarX * (Math.random() - 0.5);
      velocities[i4 + 1] = settings.behavior.vecSpeedY + settings.behavior.vecSpeedVarY * (Math.random() - 0.5);
      velocities[i4 + 2] = settings.behavior.vecSpeedZ + settings.behavior.vecSpeedVarZ * (Math.random() - 0.5);
      velocities[i4 + 3] = 1.0;
    }
    let texture = new THREE.DataTexture(velocities, width, height, THREE.RGBAFormat, THREE.FloatType);
    texture.minFilter = THREE.NearestFilter;
    texture.magFilter = THREE.NearestFilter;
    texture.needsUpdate = true;
    texture.generateMipmaps = false;
    texture.flipY = false;
    return texture;
  }

  update = (delta, time) => {

    let r = 200;
    let h = 60;

    initAnimation = Math.min(initAnimation + delta * 0.00025, 1);

    let autoClearColor = renderer.autoClearColor;
    let clearColor = renderer.getClearColor().getHex();
    let clearAlpha = renderer.getClearAlpha();

    renderer.autoClearColor = false;

    let ratio = delta / 16.6667;

    positionRawShader.uniforms.timeEffect.value = ratio;
    velocityRawShader.uniforms.dieSpeed.value = settings.behavior.dieSpeed * ratio;
    velocityRawShader.uniforms.vecSpeed.value.x = settings.behavior.vecSpeedX;
    velocityRawShader.uniforms.vecSpeed.value.y = settings.behavior.vecSpeedY;
    velocityRawShader.uniforms.vecSpeed.value.z = settings.behavior.vecSpeedZ;
    velocityRawShader.uniforms.vecSpeedVar.value.x = settings.behavior.vecSpeedVarX;
    velocityRawShader.uniforms.vecSpeedVar.value.y = settings.behavior.vecSpeedVarY;
    velocityRawShader.uniforms.vecSpeedVar.value.z = settings.behavior.vecSpeedVarZ;
    velocityRawShader.uniforms.gravity.value = settings.behavior.gravity * ratio;

    this.updateVelocity(delta);

    positionRawShader.uniforms.timeEffect.value = ratio;
    positionRawShader.uniforms.speed.value = settings.behavior.speed * ratio;
    positionRawShader.uniforms.dieSpeed.value = settings.behavior.dieSpeed * ratio;
    positionRawShader.uniforms.emitterSize.value = settings.emitter.size;
    positionRawShader.uniforms.curlSize.value = settings.behavior.curlSize;
    positionRawShader.uniforms.curlStrength.value = settings.behavior.curlStrength * ratio;
    positionRawShader.uniforms.attractionFactor.value = settings.behavior.attractionFactor * ratio;
    positionRawShader.uniforms.attractionStrength.value = settings.behavior.attractionStrength * ratio;
    positionRawShader.uniforms.initAnimation.value = initAnimation;
    positionRawShader.uniforms.mouse3d.value.copy(settings.mouse3d);

    this.updatePosition(delta);

    renderer.setClearColor(clearColor, clearAlpha);
    renderer.autoClearColor = autoClearColor;

    prevPositionRenderTarget = positionRenderTarget2;

  }

}


export {
  positionRenderTarget,
  positionRenderTarget2,
  prevPositionRenderTarget,
  textureDefaultColor,
  initAnimation,
  emitterTypes
}

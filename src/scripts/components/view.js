
const scene = new THREE.Scene();
const renderer = new THREE.WebGLRenderer({
  antialias : true
});
const camera = new THREE.PerspectiveCamera(45, 1, 10, 3000);
const prefix = 'precision ' + renderer.capabilities.precision + ' float;\n';

const clock = new THREE.Clock();
const stats = new Stats();

import settings from 'core/settings';
import Simulator from './simulator';
import particles from './particles';
import {
  updateParticles,
  SHADER_TYPE_COLORS,
  SHADER_TYPE_IMAGE
} from './particles';
import Lights from './lights';
import OrbitControls from 'controls/OrbitControls';
import Postprocess from 'components/postprocess/'

let ray = new THREE.Ray();
let controls,
  simulator,
  lights,
  colorMap,
  postprocess,
  now,
  logo,
  uniforms;
let tick = 0,
    time = 0;

const toRadian = Math.PI/180;

const loadImage = (value) => {
  let img = new Image();
  img.addEventListener('load', (e) => {
    let {width, height} = img;
    let canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    let ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, width, height);
    let imageData = ctx.getImageData(0, 0, width, height);



    simulator.updateColorMap(imageData);
    simulator.changeEmitterTo(settings.emitter.type, settings.emitter.mode);
    updateParticles(SHADER_TYPE_IMAGE);

  });
  img.src = value;
}

const onWindowResize = () => {
  let width = window.innerWidth,
    height = window.innerHeight;

//console.log(width, height)

  camera.aspect = width / height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
  postprocess.resize(width,height);
}

export default class View {

  constructor(el) {
    this.el = el;
    this.load();
    // add stats
    this.el.appendChild(stats.dom);
    // add renderer to dom
    this.el.appendChild(renderer.domElement);
    // set time
    time = Date.now();
  }

  init(){
    // initialze renderer
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(settings.rendering.backgroundColor);
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.shadowMap.enabled = true;
    window.addEventListener('resize', onWindowResize, false);

    // initialze camera
    camera.position.set(0, 60, 300).normalize().multiplyScalar(1000);
    settings.camera = camera;
    settings.cameraPosition = camera.position;

    //
    postprocess = new Postprocess();
    onWindowResize();

    // initialze controls
    controls = new OrbitControls( camera, renderer.domElement );
    controls.target.y = 50;
    controls.maxDistance = 1000;
    controls.minPolarAngle = 0.3;
    controls.maxPolarAngle = Math.PI / 2 - 0.1;
    controls.noPan = true;
    controls.update();
    // create simulator
    simulator = new Simulator({colorMap});

    // create paritcles
    // particles = new Particles();
    let container = new THREE.Object3D();
    let mesh = particles.init();
    container.add(mesh);
    scene.add(container);

    settings.mouse = new THREE.Vector2(0,0);
    settings.mouse3d = ray.origin;

    /** Add lights **/
    lights = new Lights()
    if(settings.particle.useImage){
      loadImage(settings.particle.imageName);
    }
  }

  updateLogo = () => {
    let map = {
      'logo' : 'logo',
      'outline' : 'logo_outlined',
      'mask' : 'logo_masked'
    }
    _.each(logo.children, (object) => {
      object.material.color = new THREE.Color(settings.logo.logoColor);
      object.visible = object.name === map[settings.logo.shape];
    });

  }

  load = () => {

    let manager = new THREE.LoadingManager(() => {
      logo.scale.set(100, 100, 100);
      logo.rotation.y = -90 * toRadian;
      logo.rotation.x = 90 * toRadian;
      logo.rotation.z = -270 * toRadian;
      logo.position.y = 100;
      scene.add( logo );
      //let material = new THREE.MeshBasicMaterial({map:textureDefaultColor});
      let material = new THREE.MeshPhongMaterial({color:settings.logo.logoColor});

      material.side = THREE.DoubleSide;
      _.each(logo.children, (object) => {
        object.rotation.y= 90 * Math.PI/180;
        object.material = material;
      });
      this.updateLogo();
      this.init();
      this.animate();
    });

    let colladaLoader = new THREE.ColladaLoader(manager);
    // loader.options.convertUpAxis = true;
    colladaLoader.load('geometry/wp-logo-all.dae', (collada) => {
      logo = collada.scene;
      window.logo = logo;
    });
  }

  tick = (delta, time) => {

    let {
      positionX,
      positionY,
      positionZ,
      rotationX,
      rotationY,
      rotationZ,
      size
    } = settings.logo;

    logo.position.set(positionX, positionY, positionZ);
    logo.rotation.set(rotationX * toRadian, rotationY * toRadian, rotationZ * toRadian);
    logo.scale.set(size, size, size);

    controls.update();
    simulator.update(delta, time);
    particles.update(delta, time);
    lights.update();
    camera.updateMatrixWorld();
    this.render(delta, time);
  }

  animate = () => {
    now = Date.now();
    requestAnimationFrame(this.animate);
    let delta = now - time;
    let { timeScale } = settings.rendering;
    this.tick(delta * timeScale, now);
    stats.update();
    time = now;
  }

  render = (delta, time) => {
    postprocess.render(delta, time);
    //renderer.render(scene, camera);

  }
}


export {
  camera,
  simulator,
  prefix,
  renderer,
  scene,
  logo,
  postprocess,
  loadImage
}

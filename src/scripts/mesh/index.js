import settings from 'core/settings';
import {
  particleShader,
  positionRawShader,
  triangleShader,
  distanceShader
} from 'materials/';

import {prefix} from 'components/view';

let {
  width,
  height
} = settings.getSize();

const AMOUNT = width * height;

let undef;

const createTriangleMesh = () => {

  let position = new Float32Array(AMOUNT * 3 * 3);
  let positionFlip = new Float32Array(AMOUNT * 3 * 3);
  let fboUV = new Float32Array(AMOUNT * 2 * 3);
  let PI = Math.PI;
  let angle = PI * 2 / 3;
  let angles = [
        Math.sin(angle * 2 + PI),
        Math.cos(angle * 2 + PI),
        Math.sin(angle + PI),
        Math.cos(angle + PI),
        Math.sin(angle * 3 + PI),
        Math.cos(angle * 3 + PI),
        Math.sin(angle * 2),
        Math.cos(angle * 2),
        Math.sin(angle),
        Math.cos(angle),
        Math.sin(angle * 3),
        Math.cos(angle * 3)
    ]
  let i6, i9;
  for (let i = 0; i < AMOUNT; i++) {
    i6 = i * 6;
    i9 = i * 9;
    if (i % 2) {
      position[i9 + 0] = angles[0];
      position[i9 + 1] = angles[1];
      position[i9 + 3] = angles[2];
      position[i9 + 4] = angles[3];
      position[i9 + 6] = angles[4];
      position[i9 + 7] = angles[5];

      positionFlip[i9 + 0] = angles[6];
      positionFlip[i9 + 1] = angles[7];
      positionFlip[i9 + 3] = angles[8];
      positionFlip[i9 + 4] = angles[9];
      positionFlip[i9 + 6] = angles[10];
      positionFlip[i9 + 7] = angles[11];
    } else {
      positionFlip[i9 + 0] = angles[0];
      positionFlip[i9 + 1] = angles[1];
      positionFlip[i9 + 3] = angles[2];
      positionFlip[i9 + 4] = angles[3];
      positionFlip[i9 + 6] = angles[4];
      positionFlip[i9 + 7] = angles[5];

      position[i9 + 0] = angles[6];
      position[i9 + 1] = angles[7];
      position[i9 + 3] = angles[8];
      position[i9 + 4] = angles[9];
      position[i9 + 6] = angles[10];
      position[i9 + 7] = angles[11];
    }

    fboUV[i6 + 0] = fboUV[i6 + 2] = fboUV[i6 + 4] = (i % width) / width;
    fboUV[i6 + 1] = fboUV[i6 + 3] = fboUV[i6 + 5] = ~~(i / width) / height;
  }

  let geometry = new THREE.BufferGeometry();
  geometry.addAttribute('position', new THREE.BufferAttribute(position, 3));
  geometry.addAttribute('positionFlip', new THREE.BufferAttribute(positionFlip, 3));
  geometry.addAttribute('fboUV', new THREE.BufferAttribute(fboUV, 2));

  let mesh = new THREE.Mesh(geometry, triangleShader);
  mesh.material.name = "colorShader";
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  return mesh;
};

const createParticleMesh = () => {

  let position = new Float32Array(AMOUNT * 3);
  let i3;

  for (let i = 0; i < AMOUNT; i++) {
    i3 = i * 3;
    position[i3 + 0] = (i % width) / width;
    position[i3 + 1] = ~~(i / width) / height;
  }
  let geometry = new THREE.BufferGeometry();
  geometry.addAttribute('position', new THREE.BufferAttribute(position, 3));

  let mesh = new THREE.Points(geometry, particleShader);
  mesh.castShadow = true;
  mesh.receiveShadow = true;

  return mesh;
};

const triangleMesh = createTriangleMesh();

const particleMesh = createParticleMesh();



/** get edge data of a geometry **/


/** get vertex data of a geometry **/






export {
  triangleMesh,
  createTriangleMesh,
  particleMesh,
  createParticleMesh
}

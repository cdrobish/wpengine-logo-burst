import {
  scene
} from './view';

import settings from 'core/settings';

let pointLight;
let _shadowDarkness = 0.5;

export default class Lights {
  constructor(){

    let mesh = new THREE.Object3D();
    mesh.position.set(0, 200, 1000);

    // let ambient = new THREE.AmbientLight( 0x333333 );
    // mesh.add( ambient );

    pointLight = new THREE.PointLight( 0xffffff, 100);
    pointLight.castShadow = true;
    pointLight.shadow.camera.near = 10;
    pointLight.shadow.camera.far = 7000;
    // pointLight.shadowCameraFov = 90;
    pointLight.shadow.bias = 0.1;
    // pointLight.shadowDarkness = 0.45;
    pointLight.shadow.mapSize.width = 4096;
    pointLight.shadow.mapSize.height = 2048;
    mesh.add( pointLight );

    scene.add(mesh);
  }
  update = () => {
    let {
      pointLightColor,
      shadowDarkness,
      pointLightIntensity
     } = settings.rendering;

    pointLight.color = new THREE.Color(pointLightColor);
    pointLight.shadow.darkness = shadowDarkness;
    pointLight.intensity = pointLightIntensity;
    // console.log(pointLight.shadowDarkness)
  }
}

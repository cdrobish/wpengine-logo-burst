uniform vec2 resolution;
uniform sampler2D texturePosition;
uniform sampler2D textureVelocity;
uniform float time;
uniform float timeEffect;
uniform float dieSpeed;
uniform vec3 vecSpeed;
uniform vec3 vecSpeedVar;
uniform float gravity;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {

    vec2 uv = gl_FragCoord.xy / resolution.xy;

    vec4 positionInfo = texture2D( texturePosition, uv );
    float life = positionInfo.a - dieSpeed;

    //vec3 followPosition = mix(vec3(0.0, -(1.0 - initAnimation) * 200.0, 0.0), mouse3d, smoothstep(0.2, 0.7, initAnimation));

    vec4 velocityInfo = texture2D( textureVelocity, uv );
    vec3 velocity = velocityInfo.xyz;

    if(life < 0.0) {
        vec2 seedX = uv * 0.1;
        vec2 seedY = uv * 0.2;
        vec2 seedZ = uv * 0.3;
        float xVar = vecSpeedVar.x * (rand(seedX) - 0.5);
        float yVar = vecSpeedVar.y * (rand(seedY) - 0.5);
        float zVar = vecSpeedVar.z * (rand(seedZ) - 0.5);
        velocity = (vecSpeed + vec3(xVar, yVar, zVar));
    } else {
        //vec3 delta = followPosition - position;
        velocity = velocityInfo.xyz;
        velocity.y += gravity * 0.05 * timeEffect;
        //position += delta * (0.005 + life * 0.01) * attraction * (1.0 - smoothstep(50.0, 350.0, length(delta))) *speed;
        //position += curl(position * 0.01, time, 0.1 + (1.0 - life) * 0.1) *speed;
    }

    gl_FragColor = vec4(velocity, 1.0);

}

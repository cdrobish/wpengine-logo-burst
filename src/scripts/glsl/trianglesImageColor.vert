uniform sampler2D texturePosition;
uniform sampler2D textureDefaultColor;
varying vec4 vColor;

attribute vec3 positionFlip;
attribute vec2 fboUV;

uniform float flipRatio;
uniform mat4 cameraMatrix;

void main() {

    vec4 positionInfo = texture2D( texturePosition, fboUV );
    vec4 colorInfo = texture2D( textureDefaultColor, fboUV );
    vec3 pos = positionInfo.xyz;

    vec4 worldPosition = modelMatrix * vec4( pos, 1.0 );
    vec4 mvPosition = viewMatrix * worldPosition;

    vColor = colorInfo;

    mvPosition += vec4((position + (positionFlip - position) * flipRatio) * smoothstep(0.0, 0.2, positionInfo.w), 0.0);
    gl_Position = projectionMatrix * mvPosition;
    worldPosition = cameraMatrix * mvPosition;
}

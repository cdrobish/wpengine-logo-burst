import _ from 'lodash'
import {TweenMax, TimelineMax, Quad, Expo, Cubic} from 'gsap';
import settings from 'core/settings';

const timeline = (gui) => {

  let {
    logo,
    emitter,
    behavior,
    bloom,
    motionBlur,
    rendering
  } = settings;

  const tl = new TimelineMax({
    onStart:() => {
      let children = tl.getChildren();
      _.each(children, (tl) => {
        tl.vars.vecSpeedX = _.random(-100, 100);
        tl.vars.vecSpeedY = _.random(-100, 100);
      });
      //windLeft.updateTo({vecSpeedX:_.random(-50, -10)}, true);
      //windRight.updateTo({vecSpeedX:_.random(50, 10)}, true);
    }
  });

  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});
  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});
  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});
  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});
  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});
  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});
  tl.to(behavior, 1, {vecSpeedX:0, vecSpeedY:0});

  // console.log(tl.getChildren()[0])

  // tl.to(behavior, 1, {vecSpeedY:-30});
  // tl.to(behavior, 1, {vecSpeedY:30});

  return tl;
}

export default timeline;

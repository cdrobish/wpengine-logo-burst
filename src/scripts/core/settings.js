class Settings  {
  getSize = () => {
    let width = 256,
      height = 256;

    return {
      width,
      height
    };
  }
}

// let settings = {
//   motionBlur:{
//     sampleCount:21
//   },
//   getSize: () => {
//     let width = 256,
//       height = 256;
//
//     return {
//       width,
//       height
//     };
//   }
// };

const settings = new Settings();
settings.motionBlur = {
  sampleCount:21
}

export default settings;

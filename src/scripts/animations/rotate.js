import {TweenMax, TimelineMax, Quad, Expo, Cubic} from 'gsap';
import settings from 'core/settings';

const timeline = (gui) => {

  let {
    logo,
    emitter,
    behavior,
    bloom,
    motionBlur,
    rendering
  } = settings;


  const tl = new TimelineMax({});
  let spinX = new TimelineMax({});
  let spinY = new TimelineMax({});
  let spinZ = new TimelineMax({});
  // spinX.to(emitter, 10, {rotationX:360, ease:Cubic.easeInOut});
  spinY.to([emitter], 10, {rotationY:30, ease:Cubic.easeInOut});
  spinY.to([emitter], 10, {rotationY:-30, ease:Cubic.easeInOut});
  spinY.to([emitter], 10, {rotationY:0, ease:Cubic.easeInOut});

  // spinY.to([logo], 10, {rotationY:30, ease:Cubic.easeInOut}, '-=30');
  // spinY.to([logo], 10, {rotationY:-30, ease:Cubic.easeInOut}, '-=30');
  // spinY.to([logo], 10, {rotationY:0, ease:Cubic.easeInOut}, '-=30');
  // spinZ.to(emitter, 10, {rotationZ:360, ease:Cubic.easeInOut});
  // tl.add(spinX, 'spin');
  tl.add(spinY, 'spin');
  // tl.add(spinZ, 'spin');
  return tl;
}

export default timeline;

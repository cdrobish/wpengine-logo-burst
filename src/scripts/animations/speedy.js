import {
  TweenMax,
  TimelineMax,
  Back,
  Quad,
  Expo,
  Cubic} from 'gsap';
import settings from 'core/settings';

const timeline = () => {
  let ease = Cubic.easeInOut;

  let {
    logo,
    emitter,
    behavior,
    bloom,
    motionBlur,
    rendering
  } = settings;

  const tl = new TimelineMax({});

  // move
  let move = new TimelineMax();

  move.to(emitter, 0.4, {positionY:0, ease:Back.easeOut});

  // move.set(emitter, {positionX:0, positionX:0});
  // move.to(emitter, 0.4, {positionZ:0, ease:Cubic.easeInOut}, '+=1');
  // move.to(emitter, 0.4, {positionY:-1000, ease:Cubic.easeInOut}, '+=1');
  // move.set(emitter, {positionY:600, positionX:0});

  // behavior
  let behave = new TimelineMax();
  behave.to(behavior, 1, {vecSpeedX:-50, ease:Back.easeInOut}, '+=3')

  tl.add(move, 'move+behave');
  tl.add(behave, 'move+behave');

  return tl;
}

export default timeline;

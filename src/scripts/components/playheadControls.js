import {TweenMax, TimelineMax, Quad, Expo, Cubic} from 'gsap';

import settings from 'core/settings';

const progress = '.player-controls .progress';
const progressBar = '.player-controls .progress-bar';
const play = '.playhead-controls .play-pause .fa-play';
const pause = '.playhead-controls .play-pause .fa-pause';
const backwards = '.playhead-controls .backwards';
const forwards = '.playhead-controls .forwards';
const repeat = '.playhead-controls .repeat';

const req = require.context("animations/", true, /^\.\/.*\.js$/);
const animations = {};

const getRelativeCoordinates = ( e, el ) => {

    var pos = {}, offset = {}, ref;

    ref = el.offsetParent;

    pos.x = !! e.touches ? e.touches[ 0 ].pageX : e.pageX;
    pos.y = !! e.touches ? e.touches[ 0 ].pageY : e.pageY;

    offset.left = el.offsetLeft;
    offset.top = el.offsetTop;

    while ( ref ) {

        offset.left += ref.offsetLeft;
        offset.top += ref.offsetTop;

        ref = ref.offsetParent;
    }

    return {
        x : pos.x - offset.left,
        y : pos.y - offset.top,
        };

}

_.each(req.keys(), (file) => {
  if(file !== './index.js') {
//    console.log(file);
    let animation = req(file);
    file = file.replace('.js', '').split('/');
    file.shift();
    file = file.join('.');
    animations[file] = animation.default;
  }
});


export default class PlayheadControls {
  constructor(gui){
    this.gui = gui;

    this.tl = new TimelineMax({
      onUpdate:this.onUpdate,
      onComplete:this.onComplete
    });

    TweenMax.set(progress, {scaleX:0});
    document.querySelector(forwards).classList.add('active');

    this.loop = settings.behavior.animationLoop || false;
    this.completed = false;

    if(this.loop){
      document.querySelector(repeat).classList.add('active');
    }

    let animationCtrl;

    for (let i in gui.__folders) {
      for (let j in gui.__folders[i].__controllers) {
        let prop = gui.__folders[i].__controllers[j].property;
        if(prop === 'animation') {
          animationCtrl = gui.__folders[i].__controllers[j]
        }
        if(prop === 'animationLoop') {
          this.animationLoopCtrl = gui.__folders[i].__controllers[j]
        }
      }
    }

    let selectTimeline = (value) => {
      let animation = animations[value];
      animationCtrl.setValue(value);
      if(_.isFunction(animation)) {
        this.tl.clear(true);
        this.tl.add(animation(gui));
        this.tl.pause();
      }
    };

    let select = document.querySelector('select');
    select.addEventListener('change', (e) => {
      let options = [].slice.call(e.currentTarget.querySelectorAll('option'));
      _.each(options, (option) => {
        if(option.selected){
          selectTimeline(option.value)
        }
      });
    });

    _.each(animations, (animation,  name) => {
      let option = document.createElement('option');
      option.text = name;
      option.value = name;
      if(settings.behavior.animation === name){
        option.selected = true;
        selectTimeline(name);
      }
      select.add(option);
    });

    this.tl.repeat((this.loop ? -1 : 0));
    this.tl.repeatDelay(0);
    this.tl.pause();

    this.addEventListeners();

    this.tl.play();
    TweenMax.set(play, {display:'none'});
    TweenMax.set(pause, {display:'inline-block'});

  }

  addEventListeners = () => {

    let seek = (e) => {
      let pos = getRelativeCoordinates(e, e.target);
      pos.x += e.target.clientWidth/2;
      let p = pos.x/e.target.clientWidth;
      this.tl.seek(p * this.tl.duration());
      let progressScale = this.tl.progress();
      TweenMax.set(progress, {scaleX:progressScale});
    }

    document.querySelector(progressBar).addEventListener('mousedown', seek, false);

    document.querySelector(progressBar).addEventListener('mouseup', seek, false);

    document.querySelector(play).addEventListener('click', () => {
      if(this.completed){
        this.completed = false;
        this.tl.seek(0);
      }
      this.tl.play();
      TweenMax.set(play, {display:'none'});
      TweenMax.set(pause, {display:'inline-block'});
    }, false);
    document.querySelector(pause).addEventListener('click', () => {
      this.tl.pause();
      TweenMax.set(pause, {display:'none'});
      TweenMax.set(play, {display:'inline-block'});
    }, false);
    document.querySelector(backwards).addEventListener('click', (e) => {
      e.currentTarget.classList.add('active');
      this.tl.reverse();
      document.querySelector(forwards).classList.remove('active');
    }, false);
    document.querySelector(forwards).addEventListener('click', (e) => {
      e.currentTarget.classList.add('active');
      document.querySelector(backwards).classList.remove('active');
    }, false);
    document.querySelector(repeat).addEventListener('click', (e) => {
      e.currentTarget.classList.toggle('active');
      this.loop = !this.loop;
      this.animationLoopCtrl.setValue(this.loop)
      this.tl.repeat((this.loop ? -1 : 0));
    }, false);
  }

  onComplete = () => {
    if(!this.loop) {
      TweenMax.set(pause, {display:'none'});
      TweenMax.set(play, {display:'inline-block'});
      this.completed = true;
      this.tl.pause();
    }
  }

  onUpdate = () => {

    let progressScale = this.tl.progress();

    TweenMax.set(progress, {scaleX:progressScale});

    for (let i in this.gui.__folders) {
      for (let j in this.gui.__folders[i].__controllers) {
        this.gui.__folders[i].__controllers[j].updateDisplay();
      }
    }
  }

}

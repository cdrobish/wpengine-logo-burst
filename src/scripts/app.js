import vertexShader from './glsl/example/example.vert';
import fragmentShader from './glsl/example/example.frag';



class BasicView {

  constructor(container){
    this.camera = new THREE.OrthographicCamera( - 1, 1, 1, - 1, 0, 1 );
		this.scene = new THREE.Scene();

    const geometry = new THREE.PlaneBufferGeometry( 2, 2 );

    this.uniforms = {
			time: { type:'t', value: 1.0 }
		};

		const material = new THREE.ShaderMaterial( {
			uniforms: this.uniforms,
			vertexShader,
			fragmentShader
		});

		const mesh = new THREE.Mesh( geometry, material );
		this.scene.add( mesh );

		this.renderer = new THREE.WebGLRenderer();
		this.renderer.setPixelRatio( window.devicePixelRatio );
		container.appendChild( this.renderer.domElement );
  }

  tick = ( timestamp ) => {
    if(!this.aniamte) {
      return;
    }
		requestAnimationFrame( this.tick );
    // console.log(this)
		this.uniforms.time.value = timestamp / 1000;
		this.renderer.render( this.scene, this.camera );
	}

  startRendering(){
    this.aniamte = true;
    requestAnimationFrame( this.tick )
  }

  stopRendering(){
    this.animate = false;
  }
}

export default class App {
  constructor(){
    const container = document.getElementById( 'container' );
    this.view = new BasicView(container);

    this.onWindowResize();
		window.addEventListener( 'resize', this.onWindowResize, false );
    this.view.startRendering();
  }
  onWindowResize = () => {
    this.view.renderer.setSize( window.innerWidth, window.innerHeight );
  }
}

new App();
